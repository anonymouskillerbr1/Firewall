#!/usr/bin/python
#-*- coding: utf-8 -*-
import subprocess
import sys

print('''\033[1;31m

❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑  ❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑❑
🇨 🇷 🇮 🇦 🇩 🇴   🇵 🇴 🇷  🇲 🇦 🇹 🇹 🇭 🇪 🇼  🇵 🇷 🇾 🇨 🇪

      ███████╗██╗██████╗ ███████╗██╗    ██╗ █████╗ ██╗     ██╗
      ██╔════╝██║██╔══██╗██╔════╝██║    ██║██╔══██╗██║     ██║
      █████╗  ██║██████╔╝█████╗  ██║ █╗ ██║███████║██║     ██║
      ██╔══╝  ██║██╔══██╗██╔══╝  ██║███╗██║██╔══██║██║     ██║
      ██║     ██║██║  ██║███████╗╚███╔███╔╝██║  ██║███████╗███████╗
      ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚══════╝╚══════╝

\033[0;0m''')
print('''\033[1;34m
➊  LIMPAR TODAS AS REGRAS DE FIREWALL
➋  LISTAR REGRAS DE FIREWALL
➌  PERMITIR PORTA
➍  DROPAR PORTA
➎  FILTRAR PORTA COM REJECT
➏  VERIFICAR TODAS AS PORTAS
➐  AJUDA
⓿  SAIR''')

opcao = int(input('➡  ESCOLHA UMA OPÇÃO: '))
if  opcao == 1:
    opcao = subprocess.call(['iptables','-F'])
    print(opcao)
    print('\033[1;32m✚  TODAS AS REGRAS DE FIREWALL FORAM LIMPAS.\n')



if opcao == 2:
    opcao = subprocess.call(['iptables','-L'])
    print(opcao)
    print('\033[1;32m✚  REGRAS LISTADAS.\n')






if opcao == 3:
    print('➡  ESPECIFIQUE A PORTA: ')
    porta = input('➡  ')
    print('➡  ESPECIFIQUE O HOST: ')
    host = str(input('➡  '))
    print(opcao)
    opcao = subprocess.call(['iptables','-A','INPUT','-p','tcp','-d',host,'--dport',porta,'-j','ACCEPT'])
    print('\033[1;32m✚  REGRA DE FIREWALL CRIADA COM SUCESSO.\n ')



if opcao == 4:
    print('➡  ESPECIFIQUE A PORTA:')
    porta = input('➡  ')
    print('➡  ESPECIFIQUE O HOST: ')
    host = str(input('➡  '))
    print(opcao)
    opcao = subprocess.call(['iptables','-A','INPUT','-p','tcp','-d',host,'--dport',porta,'-j','DROP'])
    print('\033[1;32m✚  PORTA DROPADA.\n')



if opcao == 5:
    print('➡  ESPECIFIQUE A PORTA:')
    porta = input('➡  ')
    print('➡  ESPECIFIQUE O HOST: ')
    host = str(input('➡  '))
    print(opcao)
    opcao = subprocess.call(['iptables','-A','INPUT','-p','tcp','-d',host,'--dport',porta,'-j','REJECT'])
    print('\033[1;32m✚  PORTA FILTRADA.\n')




if opcao == 6:
    print('➡  ESPECIFIQUE O HOST: ')
    host = str(input('➡  '))
    print(opcao)
    opcao = subprocess.call(['nmap','-v','-sS','-p-',host])
    print('\033[1;32m✚  VERIFICAÇÃO COMPLETA.\n')



if opcao == 7:
    print('''\033[32m

######################################
# LIMPAR TODAS AS REGRAS DE FIREWALL #
######################################
Limpa todas as regras de firewall
que foram configuradas.


######################################
#   LISTAR REGRAS DE FIREWALL        #
######################################
Listas as regras de firewall que foram
configuradas e verifica,se as regras
realmente foram limpas.


######################################
#          PERMITIR PORTA            #
######################################

Permite portas filtradas por default.



######################################
#            DROPAR PORTA            #
######################################
Dropa portas contra ataques de PING
e Portscanners.


######################################
#       FILTRAR PORTA COM REJECT     #
######################################
Filtra a porta contra ferramentas de
Portscanners

######################################
#     VERIFICAR TODAS AS PORTAS      #
######################################
Faz um SYN Scan com Nmap desde a porta
0 até a porta 65535 em rede interna ou
externa.
\033[0;0m''')








if opcao == 0:
    print('\033[31m✘  PROGRAMA FINALIZADO.')
